package ph.edu.usjr.emojify

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.io.File
import java.io.IOException

class MainActivity : AppCompatActivity() {

    val requestImageCapture = 1
    val requestStoragePermission = 1
    val fileProviderAuthority  = "com.example.android.fileprovider"

    var mImageView : ImageView? = null
    var mEmojifyButton : Button? = null
    var mShareFab : FloatingActionButton? = null
    var mSaveFab : FloatingActionButton? = null
    var mClearFab : FloatingActionButton? = null
    var mTitleTextView : TextView? = null

    var mTempPhotoPath : String? = null
    var mResults : Bitmap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mImageView = image_view as ImageView
        mEmojifyButton = emojify_button as Button
        mShareFab  = share_button as FloatingActionButton
        mSaveFab  = save_button as FloatingActionButton
        mClearFab = clear_button as FloatingActionButton
        mTitleTextView = title_text_view as TextView

        ButterKnife.bind(this)
        Timber.plant(Timber.DebugTree())

        emojify_button.setOnClickListener {
            emojifyMe()
        }

        save_button.setOnClickListener {
            saveMe()
        }

        share_button.setOnClickListener {
            shareMe()
        }

        clear_button.setOnClickListener {
            clearImage()
        }
    }


    fun emojifyMe() {

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                requestStoragePermission
            )
        } else {

            launchCamera()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            requestStoragePermission -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCamera()
                } else {

                    Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun launchCamera() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (takePictureIntent.resolveActivity(packageManager) != null) {

            var photoFile: File? = null
            try {
                photoFile = BitmapUtils.createTempImageFile(this)
            } catch (ex: IOException) {

                ex.printStackTrace()
            }

            if (photoFile != null) {

                mTempPhotoPath = photoFile.absolutePath

                val photoURI = FileProvider.getUriForFile(
                    this,
                    fileProviderAuthority,
                    photoFile
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, requestImageCapture)
            }
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == requestImageCapture && resultCode == Activity.RESULT_OK) {
            processAndSetImage()
        } else {

            BitmapUtils.deleteImageFile(this, mTempPhotoPath!!)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun processAndSetImage() {

        mEmojifyButton!!.visibility = View.GONE
        mTitleTextView!!.visibility = View.GONE
        mSaveFab!!.visibility = View.VISIBLE
        mShareFab!!.visibility = View.VISIBLE
        mClearFab!!.visibility = View.VISIBLE

        mResults = BitmapUtils.resamplePic(this, mTempPhotoPath!!)
        mResults = Emojifier.detectFacesandOverlayEmoji(this, mResults!!)
        mImageView!!.setImageBitmap(mResults)
    }

    fun saveMe() {

        BitmapUtils.deleteImageFile(this, mTempPhotoPath!!)
        BitmapUtils.saveImage(this, mResults!!)

    }

    fun shareMe() {

        BitmapUtils.deleteImageFile(this, mTempPhotoPath!!)
        BitmapUtils.saveImage(this, mResults!!)
        BitmapUtils.shareImage(this, mTempPhotoPath!!)

    }

    fun clearImage() {

        mTitleTextView!!.visibility = View.VISIBLE
        mEmojifyButton!!.visibility = View.VISIBLE
        mImageView!!.setImageResource(0)
        mSaveFab!!.hide()
        mClearFab!!.hide()
        mShareFab!!.hide()

        BitmapUtils.deleteImageFile(this, mTempPhotoPath!!)
    }

}
